import React from "react";
import useForm from "./useForm";
import validate from "./validateInfo";
import "./Form.css";

const FormSignup = (submitForm) => {
  const { handleChange, values, handleSubmit, errors } = useForm({
    submitForm,
    validate,
  });

  return (
    <div className='formContent'>
      <form className='form' onSubmit={handleSubmit}>
        <h1>Sign Up</h1>
        <div className='formInputs'>
          <label htmlFor='username' className='formLabel'>
            Username
          </label>
          <input
            id='username'
            type='text'
            name='username'
            className='formInput'
            placeholder='Enter your username'
            value={values.username}
            onChange={handleChange}
          />
          {errors.username && <p>{errors.username}</p>}
        </div>
        <div className='formInputs'>
          <label htmlFor='email' className='formLabel'>
            Email
          </label>
          <input
            id='email'
            type='email'
            name='email'
            className='formInput'
            placeholder='Enter your email'
            value={values.email}
            onChange={handleChange}
          />
          {errors.email && <p>{errors.email}</p>}
        </div>
        <div className='formInputs'>
          <label htmlFor='password' className='formLabel'>
            Password
          </label>
          <input
            id='password'
            type='password'
            name='password'
            className='formInput'
            placeholder='Enter your password'
            value={values.password}
            onChange={handleChange}
          />
          {errors.password && <p>{errors.password}</p>}
        </div>
        <div className='formInputs'>
          <label htmlFor='password2' className='formLabel'>
            Confirm Password
          </label>
          <input
            id='password2'
            type='password'
            name='password2'
            className='formInput'
            placeholder='Confirm your password'
            value={values.password2}
            onChange={handleChange}
          />
          {errors.password2 && <p>{errors.password2}</p>}
        </div>
        <button className='formInputButton' type='submit'>
          {" "}
          Sign Up
        </button>
      </form>
    </div>
  );
};

export default FormSignup;
