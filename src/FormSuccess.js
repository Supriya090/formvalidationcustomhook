import React from "react";

function FormSuccess() {
  return (
    <div className='formContent'>
      <div className='formSuccess'>We have received your request</div>
    </div>
  );
}

export default FormSuccess;
